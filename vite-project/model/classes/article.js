export class Article
{
	image = ""
	description = ""
	prix = ""
	nombreNote = ""
	noteMoyenne = ""

	constructor(image, description, prix, nombreNote, noteMoyenne)
	{
		this.image = image
		this.description = description
		this.prix = prix
		this.nombreNote = nombreNote
		this.noteMoyenne = noteMoyenne
	}

	toString()
	{
		return "Article\n{\n    image = " + this.image + 
				"\n    description = " + this.description +
				"\n    prix = " + this.prix +
				"\n    nombreNote = " + this.nombreNote +
				"\n    noteMoyenne = " + this.noteMoyenne +"\n}"
	}
}