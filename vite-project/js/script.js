import { Article } from "/model/classes/article.js"

window.addEventListener('load', function () {
  console.log('Cette fonction est exécutée une fois quand la page est chargée.');
  const listeArticleTest = []
  let articleTest = new Article("/images/article/defaut.png", "description", 50, 546, 4.5)
  listeArticleTest.push(articleTest)
  articleTest = new Article("/images/article/defaut.png", "description2", 5, 54, 4)
  listeArticleTest.push(articleTest)
  articleTest = new Article("/images/article/defaut.png", "description3", 45, 554, 3)
  listeArticleTest.push(articleTest)
  articleTest = new Article("/images/article/defaut.png", "description4", 65, 354, 5)
  listeArticleTest.push(articleTest)
  articleTest = new Article("/images/article/defaut.png", "description5", 95, 454, 3.5)
  listeArticleTest.push(articleTest)
  articleTest = new Article("/images/article/defaut.png", "description6", 55, 545, 4)
  listeArticleTest.push(articleTest)
  console.log(listeArticleTest.toString())
})