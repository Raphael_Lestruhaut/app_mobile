class article
{
	image = ""
	description = ""
	prix = ""
	nombreNote = ""
	noteMoyenne = ""

	constructor(image, description, prix, nombreNote, noteMoyenne)
	{
		this.image = image
		this.description = description
		this.prix = prix
		this.nombreNote = nombreNote
		this.noteMoyenne = noteMoyenne
	}

	toString()
	{
		return "Article{image = " + this.image + 
				"\ndescription = " + this.description +
				"\nprix = " + this.prix +
				"\nnombreNote = " + this.nombreNote +
				"\nnoteMoyenne = " + this.noteMoyenne
	}
}