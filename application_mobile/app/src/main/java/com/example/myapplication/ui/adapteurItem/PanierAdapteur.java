package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.ui.fragmentNavBar.PanierFragement;

import java.util.ArrayList;

public class PanierAdapteur extends ArrayAdapter<Pair<Article, Integer>>
{
    private final LayoutInflater inflater;
    private final PanierFragement fragement;

    static class ViewHolder
    {
        ImageView imageArticle;
        TextView nomArticle;
        TextView prixUnitaire;
        TextView description;
        TextView nombreArticle;
        TextView total;
    }

    public PanierAdapteur(Activity context, ArrayList<Pair<Article, Integer>> items, PanierFragement newFragement)
    {
        super(context, R.layout.layout_item_panier, items);
        this.inflater = LayoutInflater.from(context);
        this.fragement = newFragement;
    }

    @SuppressLint({"SetTextI18n", "ViewHolder"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View row;
        PanierAdapteur.ViewHolder vh;
        row = inflater.inflate(R.layout.layout_item_panier, parent, false);

        vh = new PanierAdapteur.ViewHolder();
        vh.imageArticle = row.findViewById(R.id.image_article);
        vh.nomArticle = row.findViewById(R.id.nomArticle);
        vh.prixUnitaire = row.findViewById(R.id.prixUnitaire);
        vh.description = row.findViewById(R.id.description);
        vh.nombreArticle = row.findViewById(R.id.nombreArticle);
        vh.total = row.findViewById(R.id.total);

        Pair<Article, Integer> pair = getItem(position);

        TextView nom = vh.nomArticle;
        nom.setText(pair.first.getNom());
        TextView prixUnitaire = vh.prixUnitaire;
        prixUnitaire.setText("Tarif unitaire: " + pair.first.getPrix() + " €");
        TextView description = vh.description;
        description.setText(pair.first.getDescriptionCourte());
        TextView nombre = vh.nombreArticle;
        nombre.setText(pair.second.toString());
        TextView total = vh.total;
        total.setText((pair.first.getPrix() * pair.second) + " €");

        ImageView boutonSup = row.findViewById(R.id.boutonSup);
        boutonSup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.user.supPanier(pair.first.getId());
                fragement.miseJour();
            }
        });


        return row;
    }
}
