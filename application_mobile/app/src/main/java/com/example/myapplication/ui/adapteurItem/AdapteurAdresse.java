package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.model.Adresse;

import java.util.ArrayList;

public class AdapteurAdresse extends ArrayAdapter<Adresse>
{
    LayoutInflater inflater;

    static class ViewHolder
    {
        TextView texte;
    }

    public AdapteurAdresse(Activity context, ArrayList<Adresse> items)
    {
        super(context, R.layout.layout_item_string, items);
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row;
        AdapteurAdresse.ViewHolder vh;
        row = inflater.inflate(R.layout.layout_item_string, parent, false);
        vh = new AdapteurAdresse.ViewHolder();
        vh.texte = row.findViewById(R.id.type);

        Adresse adresse = getItem(position);

        TextView adresseDisplay = vh.texte;
        adresseDisplay.setText(adresse.getNomAdresse());

        return row;
    }
}
