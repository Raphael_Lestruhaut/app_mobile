package com.example.myapplication.ui.fragmentNavBar;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.ui.adapteurItem.ArticleAdaptateur;
import com.example.myapplication.ui.adapteurItem.CommentaireAdapteur;
import com.example.myapplication.ui.adapteurItem.StringAdapteur;

import java.util.ArrayList;

public class ArticleFragment extends Fragment {

    private final Article article;

    ArticleFragment(Article newArticle)
    {
        this.article = newArticle;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);

        //icone favoris
        ImageView favoris = view.findViewById(R.id.favoris);
        if(MainActivity.user.favContient(this.article.getId()))
        {
            favoris.setImageResource(R.drawable.ic_baseline_favorite_24);
        }

        favoris.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.user.favContient(article.getId()))
                {
                    MainActivity.user.suppFavoris(article.getId());
                    favoris.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                }
                else
                {
                    MainActivity.user.addFavoris(article);
                    favoris.setImageResource(R.drawable.ic_baseline_favorite_24);
                }
            }
        });

        //pour mettre la combo box en place
        Spinner combobox = view.findViewById(R.id.choix_type);
        ArrayList<String> types = new ArrayList<>();
        types.add("En kit");
        types.add("Monté");
        StringAdapteur monAdapteur = new StringAdapteur(getActivity(), types);
        combobox.setAdapter(monAdapteur);
        combobox.setOnItemSelectedListener(new controleurSpinner(view));

        TextView titre = requireActivity().findViewById(R.id.text_titre_page);
        titre.setText(article.getNom());

        TextView nom = view.findViewById(R.id.nom_article);
        nom.setText(article.getNom());

        TextView prix = view.findViewById(R.id.prix_article);
        prix.setText(this.article.getPrix() + "€");

        RatingBar rating = view.findViewById(R.id.ratingBar);
        rating.setRating((float) this.article.getNoteMoyenne());

        TextView nombre_notes = view.findViewById(R.id.nombre_note_article);
        nombre_notes.setText(Integer.toString(this.article.getNombreNote()));

        ListView commentaire = view.findViewById(R.id.liste_commentaire);
        CommentaireAdapteur monAdapteurCommentaire = new CommentaireAdapteur(getActivity(),this.article.getCommentaires());
        commentaire.setAdapter(monAdapteurCommentaire);
        commentaire.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        Button ajoutPanier = view.findViewById(R.id.ajout_panier);
        ajoutPanier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.user.addPanier(article);
                Log.println(Log.ASSERT, "ajout panier", "test");
            }
        });

        return view;
    }

    private class controleurSpinner implements AdapterView.OnItemSelectedListener
    {

        private final View vue;

        controleurSpinner(View newVue)
        {
            this.vue = newVue;
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            TextView quantite = this.vue.findViewById(R.id.quantite);
            Button ajout = this.vue.findViewById(R.id.ajout_panier);
            if(i == 0)
            {
                quantite.setText("Quantité en stock: " + article.getStockKit());
                if(article.getStockKit() == 0)
                {
                    ajout.setEnabled(false);
                    quantite.setTextColor(Color.parseColor("#CC0000"));
                }
                else
                {
                    ajout.setEnabled(true);
                    quantite.setTextColor(Color.parseColor("#808080"));
                }
            }
            else if(i == 1)
            {
                quantite.setText("Quantité en stock: " + article.getStockMonte());
                if(article.getStockMonte() == 0)
                {
                    ajout.setEnabled(false);
                    quantite.setTextColor(Color.parseColor("#CC0000"));
                }
                else
                {
                    ajout.setEnabled(true);
                    quantite.setTextColor(Color.parseColor("#808080"));
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }
}


