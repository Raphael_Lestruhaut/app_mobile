package com.example.myapplication.ui.fragmentNavBar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.Commentaire;
import com.example.myapplication.model.User;
import com.example.myapplication.ui.adapteurItem.ArticleAdaptateur;

import java.util.ArrayList;
import java.util.LinkedList;

public class HomeFragment extends Fragment {

    //pour le test avant création api
    ArrayList<Article> _produitPopulaire = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_grid, container, false);

        User utilisateur1 = new User("utilisateur 1", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        User utilisateur2 = new User("utilisateur 2", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());

        ArrayList<Commentaire> commentaires1 = new ArrayList<>();
        ArrayList<Commentaire> commentaires2 = new ArrayList<>();
        commentaires2.add(new Commentaire(utilisateur1, "bruh", 5));
        commentaires2.add(new Commentaire(utilisateur2, "brah", 3.5));

        //pour le test avant création api
        this._produitPopulaire.add(new Article(1, "image 1", "nom 1", "description courte 1", "description 1", "note designeur 1", 3.5, 46, 50, 1, 2, commentaires1));
        this._produitPopulaire.add(new Article(2, "image 2", "nom 2", "description courte 2", "description 2", "note designeur 2", 4.5, 56, 60, 3, 4, commentaires2));
        this._produitPopulaire.add(new Article(3, "image 3", "nom 3", "description courte 3", "description 3", "note designeur 3", 5.5, 66, 70, 5, 6, commentaires1));
        this._produitPopulaire.add(new Article(4, "image 4", "nom 4", "description courte 4", "description 4", "note designeur 4", 6.5, 76, 80, 7, 8, commentaires2));
        this._produitPopulaire.add(new Article(5, "image 5", "nom 5", "description courte 5", "description 5", "note designeur 5", 7.5, 86, 90, 9, 10, commentaires1));
        this._produitPopulaire.add(new Article(6, "image 6", "nom 6", "description courte 6", "description 6", "note designeur 6", 1, 750, 987, 0, 25, commentaires2));
        this._produitPopulaire.add(new Article(7, "image 7", "nom 7", "description courte 7", "description 7", "note designeur 7", 2.75, 156, 87, 1000, 0, commentaires1));

        GridView listePopulaire = view.findViewById(R.id.item_grid);
        ArticleAdaptateur monAdapteur = new ArticleAdaptateur(getActivity(),this._produitPopulaire);
        listePopulaire.setAdapter(monAdapteur);
        listePopulaire.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listePopulaire.setOnItemClickListener((adapterView, view1, position, l) -> {
            //Article item = (Article) adapterView.getItemAtPosition(position);
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.nav_host_fragment, new ArticleFragment((Article) adapterView.getItemAtPosition(position)));
            ft.commit();
        });
        return view;
    }
}