package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.ui.fragmentNavBar.FavorisFragment;

import java.util.ArrayList;

public class FavorisAdapteur extends ArrayAdapter<Article>
{
    private final LayoutInflater inflate;
    private final FavorisFragment fragment;


    static class ViewHolder
    {
        TextView nomArticle;
        TextView prix;
        TextView description;
    }

    public FavorisAdapteur(Activity context, ArrayList<Article> items, FavorisFragment newFragment)
    {
        super(context, R.layout.layout_item_favoris, items);
        this.inflate = LayoutInflater.from(context);
        this.fragment = newFragment;
    }

    @SuppressLint({"ViewHolder", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row;
        FavorisAdapteur.ViewHolder vh;
        row = this.inflate.inflate(R.layout.layout_item_favoris, parent, false);

        vh = new FavorisAdapteur.ViewHolder();
        vh.nomArticle = row.findViewById(R.id.nomArticleFavoris);
        vh.prix = row.findViewById(R.id.prixFavoris);
        vh.description = row.findViewById(R.id.descriptionFavoris);

        Article a1 = getItem(position);

        TextView nomArticle = vh.nomArticle;
        nomArticle.setText(a1.getNom());
        TextView prix = vh.prix;
        prix.setText(a1.getPrix() +  "€");
        TextView description = vh.description;
        description.setText(a1.getDescription());

        ImageView trash = row.findViewById(R.id.trash);
        trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(a1.getStockMonte() > 0)
                {
                    MainActivity.user.suppFavoris(a1.getId());
                    fragment.miseJourVue();
                }
                else
                {
                    Toast.makeText(getContext(),  "Cet article n’est plus disponible", Toast.LENGTH_LONG ).show();
                }
            }
        });

        ImageView addPanier = row.findViewById(R.id.addPanier);
        addPanier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.user.addPanier(a1);
            }
        });

        return row;
    }
}
