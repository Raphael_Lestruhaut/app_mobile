package com.example.myapplication.model;

public class Adresse
{
    private String nomAdresse;
    private String nom;
    private String prenom;
    private String adresse;
    private String cp;
    private String ville;

    public Adresse(String nomAdresse, String nom, String prenom, String adresse, String cp, String ville)
    {
        this.nomAdresse = nomAdresse;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.cp = cp;
        this.ville = ville;
    }

    public String getNomAdresse()
    {
        return nomAdresse;
    }

    public void setNomAdresse(String nomAdresse)
    {
        this.nomAdresse = nomAdresse;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public String getAdresse()
    {
        return adresse;
    }

    public void setAdresse(String adresse)
    {
        this.adresse = adresse;
    }

    public String getCp()
    {
        return cp;
    }

    public void setCp(String cp)
    {
        this.cp = cp;
    }

    public String getVille()
    {
        return ville;
    }

    public void setVille(String ville)
    {
        this.ville = ville;
    }

    @Override
    public String toString() {
        return nomAdresse;
    }
}
