package com.example.myapplication.ui.fragmentNavBar;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.ui.adapteurItem.PanierAdapteur;

public class PanierFragement extends Fragment {

    private View view;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_panier, container, false);
        ListView panier = view.findViewById(R.id.panier);
        PanierAdapteur monAdapteur = new PanierAdapteur(getActivity(), MainActivity.user.getPanier(), this);
        panier.setAdapter(monAdapteur);
        panier.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        panier.setOnItemClickListener(((adapterView, view1, i, l) ->
        {
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Pair<Article, Integer> article = (Pair<Article, Integer>) adapterView.getItemAtPosition(i);
            ft.replace(R.id.nav_host_fragment, new ArticleFragment(article.first));
            ft.commit();
        }));
        TextView total = view.findViewById(R.id.totalPanier);
        total.setText(MainActivity.user.getTotalPanier() + " €");

        Button valider = view.findViewById(R.id.buttonValider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.user.getPanier().size() == 0)
                {
                    Toast.makeText(getActivity(), "Le panier doit contenir au moins un article", Toast.LENGTH_LONG).show();
                }
                else
                {
                    FragmentManager fm = getParentFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.nav_host_fragment, new AdresseFragement());
                    ft.commit();
                }
            }
        });
        return view;
    }

    @SuppressLint("SetTextI18n")
    public void miseJour()
    {
        ListView panier = view.findViewById(R.id.panier);
        PanierAdapteur monAdapteur = new PanierAdapteur(getActivity(), MainActivity.user.getPanier(), this);
        panier.setAdapter(monAdapteur);
        panier.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        panier.setOnItemClickListener(((adapterView, view1, i, l) ->
        {
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Pair<Article, Integer> article = (Pair<Article, Integer>) adapterView.getItemAtPosition(i);
            ft.replace(R.id.nav_host_fragment, new ArticleFragment(article.first));
            ft.commit();
        }));

        TextView total = view.findViewById(R.id.totalPanier);
        total.setText(MainActivity.user.getTotalPanier() + " €");
    }
}