package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.model.Commentaire;

import java.util.ArrayList;

public class CommentaireAdapteur extends ArrayAdapter<Commentaire> {

    private LayoutInflater inflater;

    static class ViewHolder
    {
        TextView nomCommentaire;
        TextView commentaire;
        RatingBar note;
    }

    public CommentaireAdapteur(Activity context, ArrayList<Commentaire> items)
    {
        super(context, R.layout.layout_commentaire, items);
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint({"SetTextI18n", "ViewHolder"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {

        View row;
        CommentaireAdapteur.ViewHolder vh;
        row = inflater.inflate(R.layout.layout_commentaire, parent,false);

        vh = new CommentaireAdapteur.ViewHolder();
        vh.nomCommentaire = row.findViewById(R.id.nom_commentaire);
        vh.commentaire = row.findViewById(R.id.commentaire);
        vh.note = row.findViewById(R.id.note_commentaire);

        Commentaire a1 = getItem(position);

        TextView nomCommentaire = vh.nomCommentaire;
        nomCommentaire.setText(a1.getAuteur().getPseudo());
        TextView commentaire = vh.commentaire;
        commentaire.setText(a1.getTexte());
        RatingBar note = vh.note;
        note.setRating((float) a1.getNote());

        return row;
    }
}
