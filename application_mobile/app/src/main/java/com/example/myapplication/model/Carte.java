package com.example.myapplication.model;

import java.util.Calendar;

public class Carte
{
    private String titulaire;
    private String numero;
    private Calendar dateExpi;
    private String nomCarte;

    public Carte(String titulaire, String numero, Calendar dateExpi, String nomCarte)
    {
        this.titulaire = titulaire;
        this.numero = numero;
        this.dateExpi = dateExpi;
        this.nomCarte = nomCarte;
    }

    public String getTitulaire()
    {
        return titulaire;
    }

    public void setTitulaire(String titulaire)
    {
        this.titulaire = titulaire;
    }

    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String numero)
    {
        this.numero = numero;
    }

    public Calendar getDateExpi()
    {
        return dateExpi;
    }

    public void setDateExpi(Calendar dateExpi)
    {
        this.dateExpi = dateExpi;
    }

    public String getNomCarte()
    {
        return nomCarte;
    }

    public void setNomCarte(String nomCarte)
    {
        this.nomCarte = nomCarte;
    }

    @Override
    public String toString()
    {
        return this.nomCarte;
    }
}
