package com.example.myapplication.ui.fragmentNavBar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Adresse;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.Carte;
import com.example.myapplication.ui.adapteurItem.CarteAdapteur;

import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


//TODO recupéré les adresse de la page précédente
public class CarteFragement extends Fragment
{
    private Adresse adresseLivraison;
    private Adresse adresseFacturation;

    public CarteFragement(Adresse newAdresseLivraison, Adresse newAdresseFacturation)
    {
        this.adresseLivraison = newAdresseLivraison;
        this.adresseFacturation = newAdresseFacturation;
    }

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_carte, container, false);
        TextView total = view.findViewById(R.id.totalPanier);
        total.setText(Double.toString(MainActivity.user.getTotalPanier()) + "€");

        Spinner combobox = view.findViewById(R.id.listeCarte);
        ArrayList<Carte> liste = new ArrayList<Carte>();
        Calendar vide = Calendar.getInstance();
        liste.add(new Carte("", "", vide , "Cartes existantes"));
        liste.addAll(MainActivity.user.getCartes());
        CarteAdapteur monAdapteur = new CarteAdapteur(getActivity(), liste);
        combobox.setAdapter(monAdapteur);

        combobox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View viewItem, int position, long id) {
                if(position == 0)
                {
                    TextView titulaire = view.findViewById(R.id.nomTitulaire);
                    titulaire.setText("");
                    TextView numeroCarte = view.findViewById(R.id.numeroCarte);
                    numeroCarte.setText("");
                    TextView expiDate = view.findViewById(R.id.expiDate);
                    expiDate.setText("");
                    TextView cvv = view.findViewById(R.id.cvv);
                    cvv.setText("");
                    TextView nomCarte = view.findViewById(R.id.nomCarte);
                    nomCarte.setText("");
                }
                else
                {
                    Carte carte = (Carte) parent.getItemAtPosition(position);
                    TextView titulaire = view.findViewById(R.id.nomTitulaire);
                    titulaire.setText(carte.getTitulaire());
                    TextView numeroCarte = view.findViewById(R.id.numeroCarte);
                    numeroCarte.setText(carte.getNumero());
                    TextView expiDate = view.findViewById(R.id.expiDate);
                    //+1 car dans calendar les mois sont numéroté de 0 à 11
                    expiDate.setText((carte.getDateExpi().get(Calendar.MONTH) + 1) + "/" + (carte.getDateExpi().get(Calendar.YEAR)%1000));
                    TextView cvv = view.findViewById(R.id.cvv);
                    cvv.setText("");
                    TextView nomCarte = view.findViewById(R.id.nomCarte);
                    nomCarte.setText(carte.getNomCarte());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button valider = view.findViewById(R.id.buttonValider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Boolean verification = true;
                RadioButton carte = view.findViewById(R.id.radioCarte);
                RadioButton paypal = view.findViewById(R.id.radioPaypal);
                TextView titulaire = view.findViewById(R.id.nomTitulaire);
                TextView numeroCarte = view.findViewById(R.id.numeroCarte);
                TextView expiDate = view.findViewById(R.id.expiDate);
                TextView cvv = view.findViewById(R.id.cvv);
                CheckBox memoire = view.findViewById(R.id.memoriseCarte);
                TextView nomCarte = view.findViewById(R.id.nomCarte);
                if(carte.isChecked() || paypal.isChecked())
                {
                    if(carte.isChecked())
                    {
                        if(titulaire.getText().toString().length() == 0)
                        {
                            Toast.makeText(getContext(), "Le nom du titulaire ne peut pas être vide", Toast.LENGTH_LONG).show();
                            verification = false;
                        }
                        if(numeroCarte.getText().toString().length() == 0)
                        {
                            Toast.makeText(getContext(), "Le numéro de carte ne peut pas être vide", Toast.LENGTH_LONG).show();
                            verification = false;
                        }
                        if(expiDate.getText().toString().length() == 0)
                        {
                            Toast.makeText(getContext(), "La date d'expiration ne peut pas être vide", Toast.LENGTH_LONG).show();
                            verification = false;
                        }
                        if(cvv.getText().toString().length() == 0)
                        {
                            Toast.makeText(getContext(), "Le CVV ne peut pas être vide", Toast.LENGTH_LONG).show();
                            verification = false;
                        }
                        if(memoire.isChecked() && nomCarte.getText().toString().length() == 0)
                        {
                            Toast.makeText(getContext(), "Pour enregistrer la carte le nom de la carte doit pas être vide", Toast.LENGTH_LONG).show();
                            verification = false;
                        }
                    }
                }
                else
                {
                    Toast.makeText(getContext(), "Vous devez selectioner un mode de paiement", Toast.LENGTH_LONG).show();
                    verification = false;
                }
                if(verification)
                {
                    if(memoire.isChecked())
                    {
                        String[] dateString = expiDate.getText().toString().split("/");
                        Calendar date = Calendar.getInstance();
                        date.set(Integer.parseInt(dateString[1]), Integer.parseInt(dateString[0]) - 1, 1);
                        MainActivity.user.addCarte(new Carte(titulaire.getText().toString(),numeroCarte.getText().toString(), date,nomCarte.getText().toString()));
                    }
                    FragmentManager fm = getParentFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.nav_host_fragment, new ResultatFragment(false));
                    ft.commit();
                }
            }
        });
        return view;
    }
}
