package com.example.myapplication.model;

public class Commentaire {

    private User auteur;
    private String texte;
    private double note;

    public Commentaire(User newAuteur, String newTexte, double newNote)
    {
        this.auteur = newAuteur;
        this.texte = newTexte;
        this.note = newNote;
    }

    public User getAuteur() {
        return auteur;
    }

    public void setAuteur(User auteur) {
        this.auteur = auteur;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Commentaire{" +
                "auteur=" + auteur +
                ", texte='" + texte + '\'' +
                ", note=" + note +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commentaire that = (Commentaire) o;

        if (Double.compare(that.note, note) != 0) return false;
        if (auteur != null ? !auteur.equals(that.auteur) : that.auteur != null) return false;
        return texte != null ? texte.equals(that.texte) : that.texte == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = auteur != null ? auteur.hashCode() : 0;
        result = 31 * result + (texte != null ? texte.hashCode() : 0);
        temp = Double.doubleToLongBits(note);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
