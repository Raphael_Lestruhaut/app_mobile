package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.model.SousCategorie;

import java.util.ArrayList;

public class SousCategorieAdaptateur extends ArrayAdapter<SousCategorie>
{
    LayoutInflater inflater;

    static class ViewHolder
    {
        ImageView imageSousCategorie;
        TextView nomSousCategorie;
    }

    public SousCategorieAdaptateur(Activity context, ArrayList<SousCategorie> items)
    {
        super(context, R.layout.layout_item_sous_categorie, items);
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint({"SetTextI18n", "ViewHolder"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {

        View row;
        ViewHolder vh;
        row = inflater.inflate(R.layout.layout_item_sous_categorie, parent,false);

        vh = new ViewHolder();
        vh.imageSousCategorie = row.findViewById(R.id.image_sous_categorie);
        vh.nomSousCategorie = row.findViewById(R.id.nom_sous_categorie);

        SousCategorie a1 = getItem(position);

        //TODO: récupération des images à faires
        TextView sousCategorieNom = vh.nomSousCategorie;
        sousCategorieNom.setText(a1.getNom());

        return row;
    }
}
