package com.example.myapplication.ui.fragmentNavBar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Adresse;
import com.example.myapplication.ui.adapteurItem.AdapteurAdresse;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdresseFragement extends Fragment
{
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_livraison, container, false);

        Spinner combobox = view.findViewById(R.id.listeAdresse);
        Spinner comboboxFacturation = view.findViewById(R.id.listeAdresseFacturation);
        ArrayList<Adresse> liste = new ArrayList<Adresse>();
        liste.add(new Adresse("Liste des adresses","","","","", ""));
        liste.addAll(MainActivity.user.getAdresses());
        AdapteurAdresse monAdapteur = new AdapteurAdresse(getActivity(), liste);
        combobox.setAdapter(monAdapteur);
        comboboxFacturation.setAdapter(monAdapteur);

        combobox.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View viewItem, int position, long id) {
                if(position == 0)
                {
                    Adresse adresse = (Adresse) parent.getItemAtPosition(position);
                    EditText nom = view.findViewById(R.id.nom);
                    nom.setText(adresse.getNom());
                    EditText prenom = view.findViewById(R.id.prenom);
                    prenom.setText(adresse.getPrenom());
                    EditText adresseTexte = view.findViewById(R.id.adresse);
                    adresseTexte.setText(adresse.getAdresse());
                    EditText cp = view.findViewById(R.id.cp);
                    cp.setText(adresse.getCp());
                    EditText ville = view.findViewById(R.id.ville);
                    ville.setText(adresse.getVille());
                    EditText nomAdresse = view.findViewById(R.id.nomAdresse);
                    nomAdresse.setText("");
                }
                else
                {
                    Adresse adresse = (Adresse) parent.getItemAtPosition(position);
                    EditText nom = view.findViewById(R.id.nom);
                    nom.setText(adresse.getNom());
                    EditText prenom = view.findViewById(R.id.prenom);
                    prenom.setText(adresse.getPrenom());
                    EditText adresseTexte = view.findViewById(R.id.adresse);
                    adresseTexte.setText(adresse.getAdresse());
                    EditText cp = view.findViewById(R.id.cp);
                    cp.setText(adresse.getCp());
                    EditText ville = view.findViewById(R.id.ville);
                    ville.setText(adresse.getVille());
                    EditText nomAdresse = view.findViewById(R.id.nomAdresse);
                    nomAdresse.setText(adresse.getNomAdresse());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        comboboxFacturation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View viewItem, int position, long id) {
                if(position == 0)
                {
                    Adresse adresse = (Adresse) parent.getItemAtPosition(position);
                    EditText nom = view.findViewById(R.id.nomFacturation);
                    nom.setText(adresse.getNom());
                    EditText prenom = view.findViewById(R.id.prenomFacturation);
                    prenom.setText(adresse.getPrenom());
                    EditText adresseTexte = view.findViewById(R.id.adresseFacturation);
                    adresseTexte.setText(adresse.getAdresse());
                    EditText cp = view.findViewById(R.id.cpFacturation);
                    cp.setText(adresse.getCp());
                    EditText ville = view.findViewById(R.id.villeFacturation);
                    ville.setText(adresse.getVille());
                    EditText nomAdresse = view.findViewById(R.id.nomAdresseFacturation);
                    nomAdresse.setText("");
                }
                else
                {
                    Adresse adresse = (Adresse) parent.getItemAtPosition(position);
                    EditText nom = view.findViewById(R.id.nomFacturation);
                    nom.setText(adresse.getNom());
                    EditText prenom = view.findViewById(R.id.prenomFacturation);
                    prenom.setText(adresse.getPrenom());
                    EditText adresseTexte = view.findViewById(R.id.adresseFacturation);
                    adresseTexte.setText(adresse.getAdresse());
                    EditText cp = view.findViewById(R.id.cpFacturation);
                    cp.setText(adresse.getCp());
                    EditText ville = view.findViewById(R.id.villeFacturation);
                    ville.setText(adresse.getVille());
                    EditText nomAdresse = view.findViewById(R.id.nomAdresseFacturation);
                    nomAdresse.setText(adresse.getNomAdresse());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button valider = view.findViewById(R.id.valider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                boolean validation = true;

                EditText nom = view.findViewById(R.id.nom);
                if(nom.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "Le nom de l'adresse de livraison ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText prenom = view.findViewById(R.id.prenom);
                if(prenom.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "Le prénom de l'adresse de livraison ne peut pas être vice", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText adresseTexte = view.findViewById(R.id.adresse);
                if(adresseTexte.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "l'adresse de livraison ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText cp = view.findViewById(R.id.cp);
                if(cp.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "Le CP de l'adresse de livraison ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText ville = view.findViewById(R.id.ville);
                if(ville.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "La ville de l'adresse de livraison ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                CheckBox memorise = view.findViewById(R.id.memorise);
                EditText nomAdresse = view.findViewById(R.id.nomAdresse);
                if(memorise.isChecked() && nomAdresse.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "Le nom de l'adresse de livraison ne peut pas être vide pour son enregistrement", Toast.LENGTH_LONG).show();
                    validation = false;
                }

                EditText nomFacturation = view.findViewById(R.id.nomFacturation);
                if(nomFacturation.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "Le nom de l'adresse de facturation ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText prenomFacturation = view.findViewById(R.id.prenomFacturation);
                if(prenomFacturation.getText().toString().length() == 0)
                {
                    Toast.makeText(getActivity(), "Le prénom de l'adresse de facturation ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText adresseTexteFacturation = view.findViewById(R.id.adresseFacturation);
                if(adresseTexteFacturation.getText().toString().length() == 0)
                {
                    Toast.makeText(getActivity(), "L'adresse de facturation ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText cpFacturation = view.findViewById(R.id.cpFacturation);
                if(cpFacturation.getText().toString().length() == 0)
                {
                    Toast.makeText(getActivity(), "Le cp de l'adresse de facturation ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                EditText villeFacturation = view.findViewById(R.id.villeFacturation);
                if(villeFacturation.getText().toString().length() == 0)
                {
                    Toast.makeText(getActivity(), "La ville de l'adresse de facturation ne peut pas être vide", Toast.LENGTH_LONG).show();
                    validation = false;
                }
                CheckBox memoriseFacturation = view.findViewById(R.id.memoriseFacturation);
                EditText nomAdresseFacturation = view.findViewById(R.id.nomAdresseFacturation);
                if(memoriseFacturation.isChecked() && nomAdresseFacturation.getText().toString().length() == 0)
                {
                    Toast.makeText(getContext(), "Le nom de l'adresse de facturation ne peut pas être vide pour son enregistrement", Toast.LENGTH_LONG).show();
                    validation = false;
                }

                Pattern p = Pattern.compile("^[0-9]*$");
                Matcher m = p.matcher(cp.getText().toString());
                Matcher mFacturation = p.matcher(cpFacturation.getText().toString());
                if(!m.matches() || !mFacturation.matches())
                {
                    Toast.makeText(getContext(),"CP doit être uniquement consituté de nombre",Toast.LENGTH_LONG).show();
                    validation = false;
                }

                if(validation)
                {
                    if(memorise.isChecked() && nomAdresse.getText().toString().length() != 0)
                    {
                        MainActivity.user.addAdresse(new Adresse(nomAdresse.getText().toString(),nom.getText().toString(),prenom.getText().toString(),adresseTexte.getText().toString(),cp.getText().toString(),ville.getText().toString()));
                    }

                    if(memoriseFacturation.isChecked() && nomAdresseFacturation.getText().toString().length() != 0)
                    {
                        MainActivity.user.addAdresse(new Adresse(nomAdresseFacturation.getText().toString(), nomFacturation.getText().toString(), prenomFacturation.getText().toString(), adresseTexteFacturation.getText().toString(), cpFacturation.getText().toString(), villeFacturation.getText().toString()));
                    }
                    FragmentManager fm = getParentFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.nav_host_fragment, new CarteFragement(new Adresse(nomAdresse.getText().toString(),nom.getText().toString(),prenom.getText().toString(),adresseTexte.getText().toString(),cp.getText().toString(),ville.getText().toString()), new Adresse(nomAdresseFacturation.getText().toString(), nomFacturation.getText().toString(), prenomFacturation.getText().toString(), adresseTexteFacturation.getText().toString(), cpFacturation.getText().toString(), villeFacturation.getText().toString())));
                    ft.commit();
                }
            }
        });

        return view;
    }
}
