package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.User;
import com.example.myapplication.ui.fragmentNavBar.ArticleFragment;

import java.util.ArrayList;

public class ArticleAdaptateur extends ArrayAdapter<Article>
{
    LayoutInflater inflater;

    static class ViewHolder
    {
        ImageView image_article_1;
        TextView nom_article_1;
        TextView description_article_1;
        TextView nombreNoteArticle_1;
        TextView prix_article_1;
        RatingBar note_moyenne;
        ImageView imageButton;
    }

    public ArticleAdaptateur(Activity context, ArrayList<Article> items)
    {
        super(context, R.layout.layout_item_articlev2, items);
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint({"SetTextI18n", "ViewHolder"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {

        View row;
        ViewHolder vh;
        row = inflater.inflate(R.layout.layout_item_articlev2, parent,false);

        vh = new ViewHolder();
        vh.image_article_1 = row.findViewById(R.id.image_article_1);
        vh.nom_article_1 = row.findViewById(R.id.nom_article_1);
        vh.description_article_1 = row.findViewById(R.id.description_article_1);
        vh.nombreNoteArticle_1 = row.findViewById(R.id.nombre_note_article_1);
        vh.prix_article_1 = row.findViewById(R.id.prix_article_1);
        vh.note_moyenne = row.findViewById(R.id.rating);
        vh.imageButton = row.findViewById(R.id.bouton_fav);

        Article a1 = getItem(position);

        //TODO: récupération des images à faires
        TextView article1nom = vh.nom_article_1;
        article1nom.setText(a1.getNom());
        TextView article1Description = vh.description_article_1;
        article1Description.setText(a1.getDescriptionCourte());
        TextView article1NombreNote = vh.nombreNoteArticle_1;
        article1NombreNote.setText(Integer.toString(a1.getNombreNote()));
        TextView article1prix = vh.prix_article_1;
        article1prix.setText(Double.toString(a1.getPrix()));
        RatingBar rating = vh.note_moyenne;
        rating.setRating((float) a1.getNoteMoyenne());
        ImageView fav = vh.imageButton;
        if(MainActivity.user.favContient(a1.getId()))
        {
            fav.setImageResource(R.drawable.ic_baseline_favorite_24);
        }

        fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(MainActivity.user.favContient(a1.getId()))
                {
                    fav.setImageResource(R.drawable.ic_baseline_favorite_border_24);
                    MainActivity.user.suppFavoris(a1.getId());
                    Log.println(Log.ASSERT, "resultat", MainActivity.user.getFavoris().toString());
                    Log.println(Log.ASSERT, "boutonimage", "suppression");
                }
                else
                {
                    fav.setImageResource(R.drawable.ic_baseline_favorite_24);
                    MainActivity.user.addFavoris(a1);
                    Log.println(Log.ASSERT, "resultat", MainActivity.user.getFavoris().toString());
                    Log.println(Log.ASSERT, "boutonimage", "ajout");
                }
            }
        });

        return row;
    }
}
