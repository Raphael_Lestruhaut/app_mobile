package com.example.myapplication.ui.fragmentNavBar;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.Commentaire;
import com.example.myapplication.model.SousCategorie;
import com.example.myapplication.model.User;

import java.util.ArrayList;
import java.util.LinkedList;


public class SearchFragment extends Fragment {

    //pour le test avant création api
    ArrayList<Article> _produitPopulaire = new ArrayList<>();

    //pour le test avant création api
    ArrayList<SousCategorie> _sousCategorie = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recherche, container, false);

        //pour le test avant création api

        User utilisateur1 = new User("utilisateur 1", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        User utilisateur2 = new User("utilisateur 2", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());


        ArrayList<Commentaire> commentaires1 = new ArrayList<>();
        ArrayList<Commentaire> commentaires2 = new ArrayList<>();
        commentaires2.add(new Commentaire(utilisateur1, "bruh", 5));
        commentaires2.add(new Commentaire(utilisateur2, "brah", 3.5));

        //pour le test avant création api
        this._produitPopulaire.add(new Article(1, "image 1", "nom 1", "description courte 1", "description 1", "note designeur 1", 3.5, 46, 50, 1, 2, commentaires1));
        this._produitPopulaire.add(new Article(2, "image 2", "nom 2", "description courte 2", "description 2", "note designeur 2", 4.5, 56, 60, 3, 4, commentaires2));
        this._produitPopulaire.add(new Article(3, "image 3", "nom 3", "description courte 3", "description 3", "note designeur 3", 5.5, 66, 70, 5, 6, commentaires1));
        this._produitPopulaire.add(new Article(4, "image 4", "nom 4", "description courte 4", "description 4", "note designeur 4", 6.5, 76, 80, 7, 8, commentaires2));
        this._produitPopulaire.add(new Article(5, "image 5", "nom 5", "description courte 5", "description 5", "note designeur 5", 7.5, 86, 90, 9, 10, commentaires1));
        this._produitPopulaire.add(new Article(6, "image 6", "nom 6", "description courte 6", "description 6", "note designeur 6", 1, 750, 987, 0, 25, commentaires2));
        this._produitPopulaire.add(new Article(7, "image 7", "nom 7", "description courte 7", "description 7", "note designeur 7", 2.75, 156, 87, 1000, 0, commentaires1));

        this._sousCategorie.add(new SousCategorie("sous catégorie 1", "image 1", this._produitPopulaire));
        this._sousCategorie.add(new SousCategorie("sous catégorie 2", "image 2", this._produitPopulaire));
        this._sousCategorie.add(new SousCategorie("sous catégorie 3", "image 3", this._produitPopulaire));
        this._sousCategorie.add(new SousCategorie("sous catégorie 4", "image 4", this._produitPopulaire));
        this._sousCategorie.add(new SousCategorie("sous catégorie 5", "image 5", this._produitPopulaire));

        //contrôleur
        Button texteSalon = view.findViewById(R.id.bouton_salon_texte);
        ImageButton imageSalon = view.findViewById(R.id.bouton_salon_image);

        texteSalon.setOnClickListener(view1 -> {
            Log.println(Log.ASSERT, "test", "bouton texte salon");
            this._affichagePageCategorie(this._sousCategorie, "Salon");
        });

        imageSalon.setOnClickListener(view12 -> {
            Log.println(Log.ASSERT, "test", "bouton texte salon");
            this._affichagePageCategorie(this._sousCategorie, "Salon");
        });

        Button texteCuisine = view.findViewById(R.id.bouton_texte_cuisine);
        ImageButton imageCuisine = view.findViewById(R.id.bouton_cuisine_image);

        texteCuisine.setOnClickListener(view13 -> {
            Log.println(Log.ASSERT, "test", "bouton texte cuisine");
            this._affichagePageCategorie(this._sousCategorie, "Cuisine");
        });

        imageCuisine.setOnClickListener(view14 -> {
            Log.println(Log.ASSERT, "test", "bouton image cuisine");
            this._affichagePageCategorie(this._sousCategorie, "Cuisine");
        });

        Button texteChambreAdulte = view.findViewById(R.id.bouton_texte_chambre_adulte);
        ImageButton imageChambreAdulte = view.findViewById(R.id.bouton_chambre_adulte);

        texteChambreAdulte.setOnClickListener(view16 -> {
            Log.println(Log.ASSERT, "test", "bouton texte chambre adulte");
            this._affichagePageCategorie(this._sousCategorie, "Chambre Adulte");
        });

        imageChambreAdulte.setOnClickListener(view15 -> {
            Log.println(Log.ASSERT, "test", "bouton image chambre adulte");
            this._affichagePageCategorie(this._sousCategorie, "Chambre Adulte");
        });

        Button texteChambreEnfant = view.findViewById(R.id.bouton_texte_chambre_enfant);
        ImageButton imageChambreEnfant = view.findViewById(R.id.bouton_chambre_enfant);

        texteChambreEnfant.setOnClickListener(view17 -> {
            Log.println(Log.ASSERT, "test", "bouton texte chambre enfant");
            this._affichagePageCategorie(this._sousCategorie, "Chambre Enfant");
        });

        imageChambreEnfant.setOnClickListener(view18 -> {
            Log.println(Log.ASSERT, "test", "bouton image chambre enfant");
            this._affichagePageCategorie(this._sousCategorie, "Chambre Enfant");
        });

        Button texteBain = view.findViewById(R.id.bouton_texte_bain);
        ImageButton ImageBain = view.findViewById(R.id.bouton_bain);

        texteBain.setOnClickListener(view19 -> {
            Log.println(Log.ASSERT, "test", "bouton texte bain");
            this._affichagePageCategorie(this._sousCategorie, "Salle de Bain");
        });

        ImageBain.setOnClickListener(view110 -> {
            Log.println(Log.ASSERT, "test", "bouton image bain");
            this._affichagePageCategorie(this._sousCategorie, "Salle de Bain");
        });

        Button texteBureau = view.findViewById(R.id.bouton_texte_bureau);
        ImageButton imageBureau = view.findViewById(R.id.bouton_bureau);

        texteBureau.setOnClickListener(view111 -> {
            Log.println(Log.ASSERT, "test", "bouton texte bureau");
            this._affichagePageCategorie(this._sousCategorie, "Bureau");
        });

        imageBureau.setOnClickListener(view112 -> {
            Log.println(Log.ASSERT, "test", "bouton image bureau");
            this._affichagePageCategorie(this._sousCategorie, "Bureau");
        });

        Button texteBiblo = view.findViewById(R.id.bouton_texte_biblotheque);
        ImageButton imageBiblo = view.findViewById(R.id.bouton_biblotheque);

        texteBiblo.setOnClickListener(view113 -> {
            Log.println(Log.ASSERT, "test", "bouton texte biblo");
            this._affichagePageCategorie(this._sousCategorie, "Biblothèque");
        });

        imageBiblo.setOnClickListener(view114 -> {
            Log.println(Log.ASSERT, "test", "bouton image biblo");
            this._affichagePageCategorie(this._sousCategorie, "Biblothèque");
        });

        Button texteExterieur = view.findViewById(R.id.bouton_texte_exterieur);
        ImageButton imageExterieur = view.findViewById(R.id.bouton_exterieur);

        texteExterieur.setOnClickListener(view115 -> {
            Log.println(Log.ASSERT, "test", "bouton texte exterieur");
            this._affichagePageCategorie(this._sousCategorie, "Exterieur");
        });

        imageExterieur.setOnClickListener(view116 -> {
            Log.println(Log.ASSERT, "test", "bouton image exterieur");
            this._affichagePageCategorie(this._sousCategorie, "Exterieur");
        });

        Button texteLuminaire = view.findViewById(R.id.bouton_texte_luminaire);
        ImageButton imageLuminaire = view.findViewById(R.id.bouton_luminaire);

        texteLuminaire.setOnClickListener(view117 -> {
            Log.println(Log.ASSERT, "test", "bouton texte luminaire");
            this._affichagePageCategorie(this._sousCategorie,"Luminaire");
        });

        imageLuminaire.setOnClickListener(view118 -> {
            Log.println(Log.ASSERT, "test", "bouton image luminaire");
            this._affichagePageCategorie(this._sousCategorie, "Luminaire");
        });

        Button texteAccessoire = view.findViewById(R.id.bouton_texte_accesoire);
        ImageButton imageAccessoire = view.findViewById(R.id.bouton_accesoire);

        texteAccessoire.setOnClickListener(view119 -> {
            Log.println(Log.ASSERT, "test", "bouton texte accessoire");
            this._affichagePageCategorie(this._sousCategorie, "Accessoire");
        });

        imageAccessoire.setOnClickListener(view120 -> {
            Log.println(Log.ASSERT, "test", "bouton image accessoire");
            this._affichagePageCategorie(this._sousCategorie, "Accessoire");
        });

        return view;
    }

    void _affichagePageCategorie(ArrayList<SousCategorie> newSousCategorie, String newNomPage){
        FragmentManager fm = getParentFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.nav_host_fragment, new CategorieFragment(newSousCategorie, newNomPage));
        ft.commit();
    }
}