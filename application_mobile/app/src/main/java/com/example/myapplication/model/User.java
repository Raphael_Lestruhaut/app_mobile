package com.example.myapplication.model;

import android.util.Pair;

import java.util.ArrayList;
import java.util.Iterator;

public class User {

    private String pseudo;

    private ArrayList<Article> favoris;

    private ArrayList<Pair<Article, Integer>> panier;

    private double totalPanier = 0;

    private ArrayList<Adresse> adresses;

    private ArrayList<Carte> cartes;

    public User(String newPseudo, ArrayList<Article> favoris, ArrayList<Pair<Article, Integer>> newPanier, ArrayList<Adresse> newAdresse, ArrayList<Carte> newCartes)
    {
        this.pseudo = newPseudo;
        this.favoris = favoris;
        this.panier = newPanier;
        this.adresses = newAdresse;
        this.cartes = newCartes;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public ArrayList<Article> getFavoris()
    {
        return favoris;
    }

    public void setFavoris(ArrayList<Article> favoris)
    {
        this.favoris = favoris;
    }

    public ArrayList<Pair<Article, Integer>> getPanier() {
        return panier;
    }

    public void setPanier(ArrayList<Pair<Article, Integer>> panier) {
        this.panier = panier;
    }

    public void addFavoris(Article newArticle)
    {
        favoris.add(newArticle);
    }

    public double getTotalPanier()
    {
        return totalPanier;
    }

    public void setTotalPanier(double totalPanier)
    {
        this.totalPanier = totalPanier;
    }

    public ArrayList<Adresse> getAdresses()
    {
        return adresses;
    }

    public void setAdresses(ArrayList<Adresse> adresses)
    {
        this.adresses = adresses;
    }

    public ArrayList<Carte> getCartes()
    {
        return cartes;
    }

    public void setCartes(ArrayList<Carte> cartes)
    {
        this.cartes = cartes;
    }

    public void suppFavoris(int idArticle)
    {
        int index = -1;
        boolean found = false;
        for(int i = 0; i < this.favoris.size(); i++)
        {
            if(this.favoris.get(i).getId() == idArticle)
            {
                index = i;
                found = true;
            }
        }
        if(found)
        {
            this.favoris.remove(index);
        }
    }

    public void addPanier(Article newArticle)
    {
        boolean found = false;
        for(int i = 0; i < this.panier.size(); i++)
        {
            if(this.panier.get(i).first.getId() == newArticle.getId())
            {
                Pair<Article, Integer> maj = new Pair<Article, Integer>(this.panier.get(i).first, this.panier.get(i).second + 1);
                this.panier.remove(i);
                this.panier.add(maj);
                found = true;
            }
        }
        if(!found)
        {
            this.panier.add(new Pair<Article, Integer>(newArticle, 1));
        }
        this.totalPanier = this.totalPanier + newArticle.getPrix();
    }

    public void supPanier(int idArticle)
    {
        int index = -1;
        boolean found = false;
        for(int i = 0; i < this.panier.size(); i++)
        {
            if(this.panier.get(i).first.getId() == idArticle)
            {
                index = i;
                found = true;
            }
        }
        if(found)
        {
            this.totalPanier = this.totalPanier - this.panier.get(index).first.getPrix();
            if(this.panier.get(index).second == 1)
            {
                this.panier.remove(index);
            }
            else
            {
                Pair<Article, Integer> pair = new Pair<Article, Integer>(this.panier.get(index).first, this.panier.get(index).second-1);
                this.panier.remove(index);
                this.panier.add(pair);
            }
        }
    }

    public boolean favContient(int idArticle)
    {
        boolean contient = false;
        Iterator<Article> ite = this.favoris.iterator();
        while(ite.hasNext())
        {
            if(ite.next().getId() == idArticle)
            {
                contient = true;
            }
        }
        return contient;
    }

    public void addAdresse(Adresse newAdresse)
    {
        this.adresses.add(newAdresse);
    }

    public void addCarte(Carte newCarte)
    {
        this.cartes.add(newCarte);
    }

    @Override
    public String toString()
    {
        return "User{" +
                "pseudo='" + pseudo + '\'' +
                ", favoris=" + favoris +
                '}';
    }
}
