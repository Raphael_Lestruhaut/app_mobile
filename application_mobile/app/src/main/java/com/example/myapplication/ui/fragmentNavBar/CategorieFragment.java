package com.example.myapplication.ui.fragmentNavBar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.SousCategorie;
import com.example.myapplication.ui.adapteurItem.SousCategorieAdaptateur;

import java.util.ArrayList;

public class CategorieFragment extends Fragment {

    private final ArrayList<SousCategorie> sousCategorie;
    private final String nomPage;

    CategorieFragment(ArrayList<SousCategorie> newSousCategorie, String newNomPage){
        this.sousCategorie = newSousCategorie;
        this.nomPage = newNomPage;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid, container, false);
        TextView test = requireActivity().findViewById(R.id.text_titre_page);
        test.setText(this.nomPage);
        GridView listePopulaire = view.findViewById(R.id.item_grid);
        SousCategorieAdaptateur monAdapteur = new SousCategorieAdaptateur(getActivity(), this.sousCategorie);
        listePopulaire.setAdapter(monAdapteur);
        listePopulaire.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listePopulaire.setOnItemClickListener((adapterView, view1, position, l) -> {
            //Article item = (Article) adapterView.getItemAtPosition(position);
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.nav_host_fragment, new SousCategorieFragment((SousCategorie) adapterView.getItemAtPosition(position)));
            ft.commit();
        });
        return view;
    }
}
