package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;
import com.example.myapplication.model.Carte;

import java.util.ArrayList;

public class CarteAdapteur extends ArrayAdapter<Carte>
{
    LayoutInflater inflater;

    static class ViewHolder
    {
        TextView text;
    }

    public CarteAdapteur(Activity context, ArrayList<Carte> items)
    {
        super(context, R.layout.layout_item_string, items);
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row;
        CarteAdapteur.ViewHolder vh;
        row = inflater.inflate(R.layout.layout_item_string, parent, false);
        vh = new CarteAdapteur.ViewHolder();
        vh.text = row.findViewById(R.id.type);

        Carte carte = getItem(position);

        TextView carteDisplay = vh.text;
        carteDisplay.setText(carte.getNomCarte());
        return row;
    }
}
