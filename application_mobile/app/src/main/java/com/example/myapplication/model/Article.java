package com.example.myapplication.model;

import java.util.ArrayList;
import java.util.Objects;

public class Article
{
    private int id;
    private String image;
    private String nom;
    private String descriptionCourte;
    private String description;
    private String noteDesigneur;
    private double noteMoyenne;
    private int nombreNote;
    private double prix;
    private int stockKit;
    private int stockMonte;
    private ArrayList<Commentaire> commentaires;

    public Article(int newId, String newImage, String newNom, String newDescriptionCourte, String newDescription, String newNoteDesigneur, double newNoteMoyenne, int newNombreNote, double newPrix, int newStockKit, int newStockMonte, ArrayList<Commentaire> newCommentaires)
    {
        this.id = newId;
        this.image = newImage;
        this.nom = newNom;
        this.descriptionCourte = newDescriptionCourte;
        this.description = newDescription;
        this.noteDesigneur = newNoteDesigneur;
        this.noteMoyenne = newNoteMoyenne;
        this.nombreNote = newNombreNote;
        this.prix = newPrix;
        this.stockKit = newStockKit;
        this.stockMonte = newStockMonte;
        this.commentaires = newCommentaires;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescriptionCourte() {
        return descriptionCourte;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNoteDesigneur() {
        return noteDesigneur;
    }

    public void setNoteDesigneur(String noteDesigneur) {
        this.noteDesigneur = noteDesigneur;
    }

    public void setDescriptionCourte(String descriptionCourte) {
        this.descriptionCourte = descriptionCourte;
    }

    public double getNoteMoyenne() {
        return noteMoyenne;
    }

    public void setNoteMoyenne(double noteMoyenne) {
        this.noteMoyenne = noteMoyenne;
    }

    public int getNombreNote() {
        return nombreNote;
    }

    public void setNombreNote(int nombreNote) {
        this.nombreNote = nombreNote;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public int getStockKit() {
        return stockKit;
    }

    public void setStockKit(int stockKit) {
        this.stockKit = stockKit;
    }

    public int getStockMonte() {
        return stockMonte;
    }

    public void setStockMonte(int stockMonte) {
        this.stockMonte = stockMonte;
    }

    public ArrayList<Commentaire> getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(ArrayList<Commentaire> commentaires) {
        this.commentaires = commentaires;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (Double.compare(article.noteMoyenne, noteMoyenne) != 0) return false;
        if (nombreNote != article.nombreNote) return false;
        if (Double.compare(article.prix, prix) != 0) return false;
        if (stockKit != article.stockKit) return false;
        if (stockMonte != article.stockMonte) return false;
        if (image != null ? !image.equals(article.image) : article.image != null) return false;
        if (nom != null ? !nom.equals(article.nom) : article.nom != null) return false;
        if (descriptionCourte != null ? !descriptionCourte.equals(article.descriptionCourte) : article.descriptionCourte != null)
            return false;
        if (description != null ? !description.equals(article.description) : article.description != null)
            return false;
        if (noteDesigneur != null ? !noteDesigneur.equals(article.noteDesigneur) : article.noteDesigneur != null)
            return false;
        return commentaires != null ? commentaires.equals(article.commentaires) : article.commentaires == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = image != null ? image.hashCode() : 0;
        result = 31 * result + (nom != null ? nom.hashCode() : 0);
        result = 31 * result + (descriptionCourte != null ? descriptionCourte.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (noteDesigneur != null ? noteDesigneur.hashCode() : 0);
        temp = Double.doubleToLongBits(noteMoyenne);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + nombreNote;
        temp = Double.doubleToLongBits(prix);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + stockKit;
        result = 31 * result + stockMonte;
        result = 31 * result + (commentaires != null ? commentaires.hashCode() : 0);
        return result;
    }
}
