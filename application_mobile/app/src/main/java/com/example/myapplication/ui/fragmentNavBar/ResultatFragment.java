package com.example.myapplication.ui.fragmentNavBar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.ui.adapteurItem.ResultatArticleAdapteur;

import java.util.ArrayList;

public class ResultatFragment extends Fragment
{

    private boolean resultat;

    public ResultatFragment(boolean newResultat)
    {
        this.resultat = newResultat;
    }


    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_paiement_resultat, container, false);

        Button autreCommande = view.findViewById(R.id.autreCommande);

        if(!resultat)
        {
            TextView titre = view.findViewById(R.id.titreResultat);
            titre.setText("Paiement refusé");
            TextView sousTitre = view.findViewById(R.id.sousTitreResultat);
            sousTitre.setText("Le paiement a été refusé par la banque");
            autreCommande.setText("Annuler la commande");
        }

        TextView total = view.findViewById(R.id.totalCommande);
        total.setText(MainActivity.user.getTotalPanier() + " €");

        ListView listeCommande = view.findViewById(R.id.listeCommande);
        ResultatArticleAdapteur monAdapteur = new ResultatArticleAdapteur(getActivity(), MainActivity.user.getPanier());
        listeCommande.setAdapter(monAdapteur);
        listeCommande.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        if(resultat)
        {
            MainActivity.user.setPanier(new ArrayList<>());
        }

        autreCommande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.user.setPanier(new ArrayList<>());
                FragmentManager fm = getParentFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.nav_host_fragment, new HomeFragment());
                ft.commit();
            }
        });

        return view;
    }
}
