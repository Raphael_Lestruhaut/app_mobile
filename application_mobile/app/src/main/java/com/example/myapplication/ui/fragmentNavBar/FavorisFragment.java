package com.example.myapplication.ui.fragmentNavBar;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.ui.adapteurItem.ArticleAdaptateur;
import com.example.myapplication.ui.adapteurItem.FavorisAdapteur;

public class FavorisFragment extends Fragment {

    private View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_favoris, container, false);
        ListView listeFavoris = view.findViewById(R.id.listeFavoris);
        FavorisAdapteur monAdapteur = new FavorisAdapteur(getActivity(), MainActivity.user.getFavoris(), this);
        listeFavoris.setAdapter(monAdapteur);
        listeFavoris.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listeFavoris.setOnItemClickListener(((adapterView, view1, i, l) ->
        {
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.nav_host_fragment, new ArticleFragment((Article) adapterView.getItemAtPosition(i)));
            ft.commit();
        }));
        return view;
    }

    public void miseJourVue()
    {
        ListView listeFavoris = view.findViewById(R.id.listeFavoris);
        FavorisAdapteur monAdapteur = new FavorisAdapteur(getActivity(), MainActivity.user.getFavoris(), this);
        listeFavoris.setAdapter(monAdapteur);
        listeFavoris.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listeFavoris.setOnItemClickListener(((adapterView, view1, i, l) ->
        {
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.nav_host_fragment, new ArticleFragment((Article) adapterView.getItemAtPosition(i)));
            ft.commit();
        }));
    }
}