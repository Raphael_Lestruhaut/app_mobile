package com.example.myapplication.model;

import java.util.ArrayList;

public class SousCategorie {
    private String nom;
    private String image;
    private ArrayList<Article> articles;

    public SousCategorie(String newNom, String newImage, ArrayList<Article> newArticles) {
        this.nom = newNom;
        this.image = newImage;
        this.articles = newArticles;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<Article> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "SousCategorie{" +
                "nom='" + nom + '\'' +
                ", image='" + image + '\'' +
                ", articles=" + articles +
                '}';
    }
}
