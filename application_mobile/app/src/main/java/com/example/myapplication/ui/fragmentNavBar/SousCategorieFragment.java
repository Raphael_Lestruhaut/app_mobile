package com.example.myapplication.ui.fragmentNavBar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.SousCategorie;
import com.example.myapplication.ui.adapteurItem.ArticleAdaptateur;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class SousCategorieFragment extends Fragment
{
    private SousCategorie sousCategorie;

    SousCategorieFragment(SousCategorie newSousCategorie)
    {
        this.sousCategorie = newSousCategorie;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid, container, false);
        GridView listePopulaire = view.findViewById(R.id.item_grid);
        ArticleAdaptateur monAdapteur = new ArticleAdaptateur(getActivity(),this.sousCategorie.getArticles());
        listePopulaire.setAdapter(monAdapteur);
        listePopulaire.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listePopulaire.setOnItemClickListener((adapterView, view1, position, l) -> {
            //Article item = (Article) adapterView.getItemAtPosition(position);
            FragmentManager fm = getParentFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.nav_host_fragment, new ArticleFragment((Article) adapterView.getItemAtPosition(position)));
            ft.commit();
        });
        return view;
    }
}
