package com.example.myapplication.ui.adapteurItem;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapplication.R;

import java.util.ArrayList;

public class StringAdapteur extends ArrayAdapter<String> {

    LayoutInflater inflater;

    static class ViewHolder
    {
        TextView texte;
    }

    public StringAdapteur(Activity context, ArrayList<String> items)
    {
        super(context, R.layout.layout_item_string, items);
        this.inflater = LayoutInflater.from(context);
    }

    @SuppressLint({"SetTextI18n", "ViewHolder"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {

        View row;
        StringAdapteur.ViewHolder vh;
        row = inflater.inflate(R.layout.layout_item_string, parent,false);

        vh = new StringAdapteur.ViewHolder();
        vh.texte = row.findViewById(R.id.type);

        String nom = getItem(position);

        TextView sousCategorieNom = vh.texte;
        sousCategorieNom.setText(nom);

        return row;
    }
}
