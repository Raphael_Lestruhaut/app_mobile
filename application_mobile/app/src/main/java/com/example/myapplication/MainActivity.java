package com.example.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.example.myapplication.model.Adresse;
import com.example.myapplication.model.Article;
import com.example.myapplication.model.Carte;
import com.example.myapplication.model.User;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;

import static com.example.myapplication.R.*;

public class MainActivity extends AppCompatActivity {

    public static User user = new User("pseudo", new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>());

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //pour passer une liste d'adresse à l'user test
        MainActivity.user.addAdresse(new Adresse("livraison","Cédric","Simard","11 rue josh","44470","Paris"));
        MainActivity.user.addAdresse(new Adresse("facturation","Arjac","Anderson","11 rue marre","35500","Rennes"));
        Calendar date = Calendar.getInstance();
        MainActivity.user.addCarte(new Carte("moi","1234 1234 1234 1234", date, "perso"));

        //Cette directive enlève la barre de titre
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(layout.activity_main);
        BottomNavigationView navView = findViewById(id.nav_view);
        NavController navController = Navigation.findNavController(this, id.nav_host_fragment);
        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            TextView titre = findViewById(id.text_titre_page);
            final int page_home = id.navigation_home;
            final int page_recherche = id.navigation_recherche;
            final int page_favoris = id.navigation_favoris;
            final int page_panier = id.navigation_panier;
            final int page_profil = id.navigation_profil;
            switch (destination.getId()){
                case page_home:
                    titre.setText(getResources().getText(string.titre_produit_populaire));
                    titre.setTextColor(Color.parseColor("#FFFFFF"));
                    break;
                case page_recherche:
                    titre.setText(getResources().getText(string.titre_recherche));
                    titre.setTextColor(Color.parseColor("#FFFFFF"));
                    break;
                case page_favoris:
                    titre.setText(getResources().getText(string.titre_favoris));
                    titre.setTextColor(Color.parseColor("#FFFFFF"));
                    break;
                case page_panier:
                    titre.setText(getResources().getText(string.titre_panier));
                    titre.setTextColor(Color.parseColor("#FFFFFF"));
                    break;
                case page_profil:
                    titre.setText(getResources().getText(string.titre_profil));
                    titre.setTextColor(Color.parseColor("#FFFFFF"));
            }
        });
        NavigationUI.setupWithNavController(navView, navController);
    }

}